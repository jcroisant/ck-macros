
This document describes the changes in each version of ck-macros.

This library follows "[semantic versioning](http://semver.org)".
Until version 1.0.0 is released, the API is not guaranteed to be "stable".
That means the maintainer reserves the right to change the API if needed,
possibly in ways that break backward compatibility with previous versions.

After version 1.0.0 is released, the API is guaranteed to remain stable (no backward-incompatible changes)
until the next new major version (e.g. going from version 1.x to 2.0.0, or 2.x to 3.0.0).


# 0.4.0 (in development)

- **Backward-incompatible change:**
  The format of the call stack (the first argument to `ck`) has changed.
  This should not affect most users, but will break
  any custom CK-macros that inspect or manipulate the call stack.
  It also affects the output of error messages.

- The `ck` macro has been enhanced to store an environment,
  i.e. variable bindings. The "CK machine" is now a "CEK machine".

- Added new portable CK-macros related to binding and reading variables:

  ```
  c-env-assoc
  c-let               c-let*
  c-var               c-var/default       c-var?
  ```

- Added the `c-error` CK-macro which immediately aborts evaluation
  and raises a syntax error. A portable implementation is provided,
  as well as a CHICKEN-specific implementation with more user-friendly output.

- Added a non-portable `c-trace` CK-macro which prints a CK stack trace,
  to aid debugging. It is considered non-portable because
  it requires `er-macro-transformer`, which is not defined in standard R5RS,
  although available in many Scheme implementations.

- Added new portable CK-macros:

  ```
  c-make-assoc        c-make-eq?
  c-dlist-ref
  ```

- Simplified/optimized several portable CK-macros.


# 0.3.1 (2024-01-27)

- **Backward incompatible change:** Due to the below bugfix, the
  `ck-macros` module must be imported into both the runtime and
  syntactic environments:

  ```scheme
  (import ck-macros)
  (import-for-syntax ck-macros)
  ```

- Fixed `unbound variable: ck-wrapper` on CHICKEN 5.2.0 and later.
  Previously, `ck-wrapper` was defined only at the syntactic level,
  which only worked by accident on older CHICKEN versions. Thanks to
  Peter Bex for the patch.


# 0.3.0 (2019-03-17)

- The library is now released under [the Unlicense](http://unlicense.org),
  to make more clear and explicit that it is released to the public domain.

- Added support for quasiquote, unquote, and unquote-splicing.
  Internally, `(quasiquote x)` (also written as <code>\`x</code>)
  is converted into `(c-quasiquote 'x)`,
  a call to the `c-quasiquote` CK-macro, which handles unquoting.

  Quasiquotation can be used as the argument to the core `ck` macro itself,
  and/or in arguments to CK-macros.
  This is useful for assembling mostly-literal forms, lists, and vectors:

  ```scheme
  (ck () `(define (foo) (list ,@(c-list '1 '2) ,(c-vector '3))))
  ;; ==> (define (foo) (list 1 2 #(3)))
  ```

- Added new portable CK-macros:

  ```
  c-quasiquote
  c-flip              c-branch
  c-constantly
  c-make-rules        c-make-next
  c-cons*             c-prefix            c-unfold
  c-vector-prefix
  c-dadd1             c-dsub1
  c-du                c-ud
  ```

- Simplified/optimized several portable CK-macros.


# 0.2.0 (2019-02-28)

- **Backward-incompatible change:** In `c-apply`,
  items in the final list should no longer have an extra level of quotes.

  ```
  Old:  (ck () (c-apply '(c-list) 'a '('b 'c)))
  New:  (ck () (c-apply '(c-list) 'a '(b c)))
                                       ^-^------ no extra quotes
  ```

* **Backward-incompatible change:** Renamed several CK-macros
  to reflect the fact that they only accept one list/vector,
  unlike the analoguous procedures which accept one or more lists/vectors.
  New CK-macros which accept more arguments may be added in the future.

  ```
  c-map           ->  c-map1
  c-append-map    ->  c-append-map1
  c-fold          ->  c-fold1
  c-any           ->  c-any1
  c-every         ->  c-every1
  c-vector-map    ->  c-vector-map1
  ```

- Ported the egg to be compatible with both CHICKEN 4 and CHICKEN 5.

- Added some new portable CK-macros:

  ```
  c-compose           c-rcompose
  c-true              c-false
  c-compare?
  c-xcons
  c-map2              c-map3              c-map4              c-map5
  ```

- Added `ck-wrapper` procedure, to wrap procedures as CK-macros.
  It is considered non-portable because it requires `er-macro-transformer`,
  which is not defined in standard R5RS,
  although available in many Scheme implementations.

- Added non-portable wrappers of many R5RS procedures:

  ```
  c-eqv?              c-eq?               c-equal?
  c-number?           c-integer?
  c-=       c-<       c->       c-<=      c->=
  c-+       c-*       c--       c-/
  c-max               c-min
  c-remainder         c-floor             c-round
  c-exact->inexact    c-inexact->exact
  c-number->string    c-string->number
  c-length            c-list-ref
  c-symbol?           c-symbol->string    c-string->symbol
  c-char?             c-char=?
  c-string?           c-string            c-string-length     c-string-ref
  c-string=?          c-string-ci=?
  c-substring         c-string-append
  c-vector-length     c-vector-ref
  ```

- Rewrote `c-last` to no longer use tail patterns, for strict R5RS portability.
  Tail patterns are a non-standard feature from
  [SRFI 46](https://srfi.schemers.org/srfi-46/srfi-46.html).

- Rewrote `c-apply` to no longer use tail patterns,
  and to no longer expect an extra level of quoting in the final list.


# 0.1.1 (2016-02-07)

Fixed `c-append` and `c-vector-append` failing if given one argument.


# 0.1.0 (2016-02-06)

Initial release.

Includes version 1.1 (April 2011) of the core `ck` macro.

Includes the following CK-macros:

```
c-quote        c-eval         c-call         c-apply        c-identity

c-if           c-if*          c-not
c-or           c-or*          c-and          c-and*
c-null?        c-pair?        c-not-pair?    c-vector?      c-boolean?
c-sym-eq?      c-sym-equal?

c-cons         c-list         c-car          c-cdr
c-first        c-second       c-third        c-fourth       c-fifth
c-sixth        c-seventh      c-eighth       c-ninth        c-tenth
c-last         c-last-pair
c-drop1        c-drop2        c-drop3        c-drop4        c-drop5
c-take1        c-take2        c-take3        c-take4        c-take5
c-reverse      c-suffix       c-append-map   c-append
c-map          c-fold         c-filter       c-remove
c-find         c-find-tail
c-member
c-any          c-every
c-assoc        c-alist-delete

c-vector            c-list->vector      c-vector->list
c-vector-reverse    c-vector-suffix     c-vector-append     c-vector-map

c-u=           c-u<           c-u<=          c-u>           c-u>=
c-uzero?       c-ueven?       c-uodd?
c-u+           c-u*           c-u-           c-u/
c-ufactorial
c-udrop        c-utake
```
