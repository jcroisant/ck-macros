default: build

build:
	chicken-install -n

install:
	chicken-install

install-test:
	chicken-install -test

uninstall:
	chicken-uninstall ck-macros

test:
	cd tests && csc run.scm && ./run

clean:
	rm -f *.import.scm *.types *.c *.o *.so *.link *.build.sh *.install.sh tests/run
