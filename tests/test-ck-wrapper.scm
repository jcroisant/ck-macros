;;; Tests for lib/ck-wrapper.scm.
;;; Depends on a testing API compatible with CHICKEN's test egg,
;;; and a procedure (pseudo-random-integer n) which returns a
;;; pseudo-random integer in the interval 0 .. n-1.


(test-group "ck-wrapper"
  (define-syntax %c-add (ck-wrapper +))
  (define-syntax %c-sub (ck-wrapper -))
  (define-syntax %c-mul (ck-wrapper *))

  (test 7 (ck () (%c-add '1 (%c-mul '2 '3))))
  (test '(4 5 6) (ck () (c-quote (c-map1 '(%c-add '1 '2) '(1 2 3)))))

  (let-syntax ((%c-add2 (ck-wrapper (lambda (x) (+ x 2)))))
    (test '(3 4 5) (ck () (c-quote (c-map1 '(%c-add2) '(1 2 3))))))

  ;; The wrapped expression is evaluated only once, so the random
  ;; number is the same for every expansion of %c-some-num.
  (let-syntax ((%c-some-num
                (ck-wrapper (let ((n (+ 100 (pseudo-random-integer 100))))
                              (lambda () n)))))
    (test 0 (ck () (%c-sub (%c-add (%c-some-num) (%c-some-num))
                           (%c-mul (%c-some-num) '2))))))
