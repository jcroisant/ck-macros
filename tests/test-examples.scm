;;; Examples from the wiki docs that are not tested elsewhere.
;;;
;;; Tests from the documentation for a specific macro usually go in
;;; the unit tests for that macro. But, the tests in test-portable.scm
;;; should be portable, so examples that use non-portable CK-macros go
;;; in this file instead.

(test-begin "documentation examples")

;;; From "Usage"
(test (vector (* 1 4) (* 2 5) (* 3 6))
      (ck () `(vector ,@(c-map2 '(c-list '*) '(1 2 3) '(4 5 6)))))


(define (print-yay)
  (ck () (c-list 'when (c-list '< '1 '2)
                 (c-list 'print '"Yay!"))))

;;; From "Combining CK and non-CK macros"
(test "Yay!\n"
      (with-output-to-string print-yay))


;;; From "Partial application"
(test '(#(2) #(3) #(4))
      (ck () (c-quote (c-map1 '(c-vector) '(2 3 4)))))

(test '(#(x y 2) #(x y 3) #(x y 4))
      (ck () (c-quote (c-map1 '(c-vector 'x 'y) '(2 3 4)))))

(test '(foo10 foo20 foo30)
      (ck () (c-quote (c-map1 '(c-rcompose
                                '((c-* '10)
                                  (c-number->string)
                                  (c-string-append '"foo")
                                  (c-string->symbol)))
                              '(1 2 3)))))


;;; From "Quasiquotation" and c-quasiquote
(ck () `(define (foo) (list ,@(c-list '1 '2) ,(c-vector '3))))
(test (list 1 2 #(3))
      (foo))

(test #(3 2 1)
      (ck () (c-vector-reverse `#(1 ,@(c-list '2 '3)))))


;;; From ck-make-rules. This example is not in test-portable.scm
;;; because it uses the non-portable macros c-+ and c--
(ck () `(define-syntax c-math
          ,(c-make-rules '(add subtract)
             '(('add 'x 'y)
               (c-+ 'x 'y))
             '(('subtract 'x 'y)
               (c-- 'x 'y)))))

(test 4 (ck () (c-quote (c-math 'add '3 '1))))
(test 2 (ck () (c-quote (c-math 'subtract '3 '1))))




(test-end)
