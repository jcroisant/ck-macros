
(module test-c-trace ()
  (import scheme)
  (cond-expand
    (chicken-4
     (import chicken)
     (use extras ports posix srfi-13 test))
    (chicken-5
     (import (chicken base)
             (chicken io)
             (chicken port)
             (chicken process)
             (chicken process-context)
             (srfi 13)
             test)))

  (define (run command . args)
    (receive (in out pid) (process command args)
      (receive (_ normal-exit? status) (process-wait pid)
        (and normal-exit?
             (zero? status)
             (string-trim
              (string-join (with-input-from-port in read-lines)
                           "\n"))))))

  (define csc (or (get-environment-variable "CHICKEN_CSC") "csc"))

  (define expected
    (string-trim "
ck stack (example-c-trace.scm:2):
    (c-trace '1)
    (c-fold1 '(c-compose '((c-trace) (c-+))) ^^^ '(2 3))
    (c-trace ^^^)
ck stack (example-c-trace.scm:2):
    (c-trace '3)
    (c-fold1 '(c-compose '((c-trace) (c-+))) ^^^ '(3))
    (c-trace ^^^)
ck stack (example-c-trace.scm:2):
    (c-trace '6)
    (c-fold1 '(c-compose '((c-trace) (c-+))) ^^^ '())
    (c-trace ^^^)
ck stack (example-c-trace.scm:2):
    (c-trace '6)
ck stack (example-c-trace.scm:6):
    (c-trace '(+))
    (c-append ^^^ (c-quasiquote '(,'1 ,@`(2 3))))"))

  (define expected-no-line-numbers
    (string-trim "
ck stack:
    (c-trace '1)
    (c-fold1 '(c-compose '((c-trace) (c-+))) ^^^ '(2 3))
    (c-trace ^^^)
ck stack:
    (c-trace '3)
    (c-fold1 '(c-compose '((c-trace) (c-+))) ^^^ '(3))
    (c-trace ^^^)
ck stack:
    (c-trace '6)
    (c-fold1 '(c-compose '((c-trace) (c-+))) ^^^ '())
    (c-trace ^^^)
ck stack:
    (c-trace '6)
ck stack:
    (c-trace '(+))
    (c-append ^^^ (c-quasiquote '(,'1 ,@`(2 3))))"))

  (test-group "c-trace"
    (cond-expand
      (chicken-5
       (test expected
             (run csc "-P" "example-c-trace.scm")))
      (chicken-4
       ;; For some reason line numbers don't work on CHICKEN 4?
       (test expected-no-line-numbers
             (run csc "-P" "example-c-trace.scm"))))))
