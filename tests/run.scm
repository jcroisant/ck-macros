;;; Test entry point for CHICKEN Scheme.

(cond-expand
  (chicken-4
   (use ck-macros test)
   (use-for-syntax ck-macros)
   (define-for-syntax pseudo-random-integer random))
  (chicken-5
   (import
    ck-macros test
    (only (chicken port) with-output-to-string))
   (import-for-syntax
    ck-macros
    (only (chicken random) pseudo-random-integer))))

(test-begin "ck-macros")

(include "test-portable.scm")
(include "test-ck-wrapper.scm")
(include "test-wrappers-r5rs.scm")
(include "test-compare.scm")
(include "test-unfold.scm")
(include "test-c-trace.scm")
(include "test-examples.scm")

(test-end "ck-macros")
(test-exit)
