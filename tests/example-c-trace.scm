(import ck-macros)
(ck () (c-trace
        (c-fold1 '(c-compose '((c-trace) (c-+)))
                 '0
                 '(1 2 3))))
(ck () `(,@(c-trace '(+)) ,'1 ,@`(2 3)))
