
;;; ck-macros
;;; Composable Scheme macros based on the CK abstract machine.
;;; Version 0.3.1 (2024-01-27)
;;;
;;; All source code (including contributions) is released by its
;;; authors to the public domain. For more information, please refer
;;; to <http://unlicense.org>

(module ck-macros *
  (import scheme)
  (cond-expand
   (chicken-4
    (import chicken)
    (import-for-syntax chicken))
   (chicken-5
    (import (chicken base)
            (chicken syntax)
            (chicken type))
    (import-for-syntax (chicken port)
                       (chicken type))))

  (include "lib/portable.scm")

  (define-type compare? (* * -> boolean))
  (define-type frame (pair list list))
  (begin-for-syntax
   (define-type compare? (* * -> boolean))
   (define-type frame (pair list list))
   (define %ck:strip-syntax strip-syntax)
   (define %ck:get-line-number get-line-number)
   (include "lib/helpers.scm"))

  (begin-for-syntax
   (include "lib/ck-wrapper.scm"))
  (include "lib/ck-wrapper.scm")        ; Include again for export
  (include "lib/wrappers-r5rs.scm")

  (include "lib/c-error.chicken.scm")
  (include "lib/c-trace.scm"))
