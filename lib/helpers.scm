
;;; ck-macros
;;; Composable Scheme macros based on the CK abstract machine.
;;; Version 0.4.0 (in development)
;;;
;;; All source code (including contributions) is released by its
;;; authors to the public domain. For more information, please refer
;;; to <http://unlicense.org>
;;;
;;; These are private, internal helper procedures, meant to be used
;;; only within the ck-macros library itself, not by library users.
;;;
;;; Credit John Croisant:
;;;
;;;   %ck:print-stack-header
;;;   %ck:print-stack-trace         %ck:print-stack-frame
;;;   %ck:write-sugar               %ck:sugar
;;;   %ck:write-sugar-pair          %ck:write-sugar-vector
;;;
;;; This code is portable to any Scheme that provides
;;; er-macro-transformer. It requires helpers from helpers.scm


;;; The following procedures should be defined as appropriate for the
;;; Scheme system. Or, use the stub implementations described below.
;;;
;;; (%ck:strip-syntax x)  ->  stripped x
;;;
;;;   Strips any syntactic information from x. E.g. if a symbol has
;;;   been renamed for macro hygiene, returns the original symbol.
;;;   See strip-syntax from CHICKEN. Stub implementation:
;;;     (define (%ck:strip-syntax x) x)
;;;
;;; (%ck:get-line-number expr)  ->  string or #f
;;;
;;;   Returns a string describing the originating source file and line
;;;   number of the expression, or #f if that information is unknown.
;;;   See get-line-number from CHICKEN. Stub implementation:
;;;     (define (%ck:get-line-number expr) #f)


;; Type declarations for helpers
(cond-expand
  (chicken-5
   (: %ck:print-stack-header
      ((or string false) output-port -> undefined))
   (: %ck:print-stack-trace
      ((list-of frame) string compare? output-port -> undefined))
   (: %ck:print-stack-frame
      (frame boolean string compare? output-port -> undefined))
   (: %ck-write-sugar
      (* compare? output-port -> undefined))
   (: %ck-sugar
      (symbol compare? --> (or string false)))
   (: %ck-write-sugar-pair
      (pair compare? output-port -> undefined))
   (: %ck-write-sugar-vector
      (vector compare? output-port -> undefined)))
  (else))


(define (%ck:print-stack-header maybe-line-number port)
  (display "ck stack" port)
  (if maybe-line-number
      (begin
        (display " (" port)
        (display maybe-line-number port)
        (display ")" port)))
  (display ":" port)
  (newline port))


(define (%ck:print-stack-trace call-stack indent compare? port)
  (%ck:print-stack-frame (car call-stack) #t indent compare? port)
  (for-each (lambda (frame)
              (%ck:print-stack-frame frame #f indent compare? port))
            (cdr call-stack)))


(define (%ck:print-stack-frame frame is-top indent compare? port)
  (let ((call (caar frame))
        (evaled (cdar frame))
        (unevaled (cdr frame)))
    (display indent port)
    (if (and is-top (null? unevaled))
        (%ck:write-sugar `(,call ,@evaled) compare? port)
        (%ck:write-sugar `(,call ,@evaled ^^^ ,@unevaled) compare? port))
    (newline port)))


;; Like write, but quote/quasiquote/etc. forms are written with
;; syntactic sugar, e.g. 'x instead of (quote x). Also, strips
;; syntax from symbols, e.g. c-cons instead of c-cons123.
(define (%ck:write-sugar x compare? port)
  (cond ((and (list? x)
              (= 2 (length x))
              (%ck:sugar (car x) compare?))
         (display (%ck:sugar (car x) compare?) port)
         (%ck:write-sugar (cadr x) compare? port))
        ((pair? x)
         (%ck:write-sugar-pair x compare? port))
        ((vector? x)
         (%ck:write-sugar-vector x compare? port))
        ((symbol? x)
         (write (%ck:strip-syntax x) port))
        (else
         (write x port))))

(define (%ck:sugar sym compare?)
  (cond ((compare? sym 'quote) "'")
        ((compare? sym 'quasiquote) "`")
        ((compare? sym 'unquote) ",")
        ((compare? sym 'unquote-splicing) ",@")
        (else #f)))

(define (%ck:write-sugar-pair x compare? port)
  (display "(" port)
  (%ck:write-sugar (car x) compare? port)
  (let loop ((x (cdr x)))
    (cond ((pair? x)
           (display " " port)
           (%ck:write-sugar (car x) compare? port)
           (loop (cdr x)))
          ((not (null? x))
           (display " . " port)
           (%ck:write-sugar x compare? port))))
  (display ")" port))

(define (%ck:write-sugar-vector x compare? port)
  (display "#(" port)
  (let loop ((i 0))
    (if (< i (vector-length x))
        (begin (if (> i 0) (display " " port))
               (%ck:write-sugar (vector-ref x i) compare? port)
               (loop (+ i 1)))))
  (display ")" port))
