
;;; ck-macros
;;; Composable Scheme macros based on the CK abstract machine.
;;; Version 0.4.0 (in development)
;;;
;;; All source code (including contributions) is released by its
;;; authors to the public domain. For more information, please refer
;;; to <http://unlicense.org>
;;;
;;; `c-error' is credit John Croisant.
;;;
;;; This is a CHICKEN Scheme-specific implementation of c-error.
;;; See c-error.portable.scm for a portable (but less user-friendly)
;;; implementation, and documentation.


(cond-expand
  (chicken-5
   (import-for-syntax
    (only (chicken port) call-with-output-string)
    (only (chicken syntax) syntax-error)))
  (chicken-4
   (use-for-syntax
    (only ports call-with-output-string))))


(define-syntax c-error
  (er-macro-transformer
   (lambda (expr rename compare?)
     (let ((call-stack (cdar (cadr expr)))
           (unquoted-args (map cadr (cddr expr)))
           (line-number (%ck:get-line-number expr))
           (indent "    "))
       (list (rename 'syntax-error)
             line-number
             (call-with-output-string
               (lambda (port)
                 (display (car unquoted-args) port)
                 (newline port)
                 (for-each (lambda (arg)
                             (write arg port)
                             (newline port))
                           (cdr unquoted-args))
                 (%ck:print-stack-header
                  line-number port)
                 (%ck:print-stack-trace
                  `(((c-error ...)) ,@call-stack)
                  indent compare? port))))))))
