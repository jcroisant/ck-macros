
;;; ck-macros
;;; Composable Scheme macros based on the CK abstract machine.
;;; Version 0.3.1 (2024-01-27)
;;;
;;; All source code (including contributions) is released by its
;;; authors to the public domain. For more information, please refer
;;; to <http://unlicense.org>
;;;
;;; `ck-wrapper' is credit John Croisant.
;;;
;;; This code is portable to any Scheme that provides
;;; er-macro-transformer.


;;; Returns a syntax transformer that wraps a procedure as a CK-macro.
;;; This can be used with e.g. define-syntax or let-syntax.
;;;
;;; PROC can be any expression that evaluates to a procedure, e.g. a
;;; procedure name or lambda form. The expression will be evaluated
;;; once, at macro expansion time, when ck-wrapper is called.
;;;
(define (ck-wrapper PROC)
  (er-macro-transformer
   (lambda (expr rename compare)
     (let ((s (cadr expr))
           (unquoted-args (map cadr (cddr expr))))
       `(,(rename 'ck) ,s ',(apply PROC unquoted-args))))))
