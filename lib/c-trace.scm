
;;; ck-macros
;;; Composable Scheme macros based on the CK abstract machine.
;;; Version 0.4.0 (in development)
;;;
;;; All source code (including contributions) is released by its
;;; authors to the public domain. For more information, please refer
;;; to <http://unlicense.org>
;;;
;;; `c-trace' is credit John Croisant.
;;;
;;; This code is portable to any Scheme that provides
;;; er-macro-transformer. It requires helpers from helpers.scm


;;; (c-trace)  ->  '#f
;;; (c-trace X MORE ...)  ->  X
;;;
;;; CK-macro debugging aid. When a call to c-trace is expanded,
;;; it prints a stack trace to the current output port.
;;; Yields the first arg, or '#f if no args are given.
;;;
;;; To generate the most useful output, you should call c-trace
;;; *within* the code you are debugging. The stack trace contains the
;;; call to c-trace itself, as well as any outer calls that are in the
;;; process of being evaluated.
;;;
;;; If possible, the output will show the originating source file name
;;; and line number of the call to {{ck}}. On CHICKEN, this is only
;;; possible when using the compiler, not the interpreter.
;;;
;;; The stack trace is printed with the inner-most call first. The
;;; marker ^^^ indicates the position of the argument currently being
;;; evaluated. In other words, ^^^ will be filled in with the value of
;;; the call above it. Values to the left of ^^^ are arguments that
;;; have already been evaluated, and expressions to the right of ^^^
;;; are arguments waiting to be evaluated.
(define-syntax c-trace
  (er-macro-transformer
   (lambda (expr rename compare?)
     (let ((stacks (cadr expr))
           (call-stack (cdar (cadr expr)))
           (args (cddr expr))
           (unquoted-args (map cadr (cddr expr)))
           (line-number (%ck:get-line-number expr))
           (port (current-output-port))
           (indent "    "))
       (%ck:print-stack-header line-number port)
       (%ck:print-stack-trace
        `(((c-trace ,@args)) ,@call-stack)
        indent compare? port)
       `(,(rename 'ck) ,stacks ',(and (pair? unquoted-args)
                                      (car unquoted-args)))))))
