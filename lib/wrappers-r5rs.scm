
;;; Composable Scheme macros based on the CK abstract machine.
;;; Version 0.3.1 (2024-01-27)
;;;
;;; All source code (including contributions) is released by its
;;; authors to the public domain. For more information, please refer
;;; to <http://unlicense.org>
;;;
;;; This file defines CK-macro wrappers for some R5RS procedures that
;;; are useful for building macros. This is not comprehensive. Users
;;; can easily define additional wrappers as needed.
;;;
;;; Any CK-macros that are POSSIBLE to implement without wrappers
;;; (e.g. c-reverse) are defined in portable.scm instead, even if a
;;; wrapper would be more efficient.
;;;
;;; This code is portable to any R5RS Scheme that supports ck-wrapper.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; GENERAL

(define-syntax c-eqv? (ck-wrapper eqv?))
(define-syntax c-eq? (ck-wrapper eq?))
(define-syntax c-equal? (ck-wrapper equal?))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; NUMBERS

(define-syntax c-number? (ck-wrapper number?))
(define-syntax c-integer? (ck-wrapper integer?))

(define-syntax c-= (ck-wrapper =))
(define-syntax c-< (ck-wrapper <))
(define-syntax c-> (ck-wrapper >))
(define-syntax c-<= (ck-wrapper <=))
(define-syntax c->= (ck-wrapper >=))

(define-syntax c-max (ck-wrapper max))
(define-syntax c-min (ck-wrapper min))

(define-syntax c-+ (ck-wrapper +))
(define-syntax c-* (ck-wrapper *))
(define-syntax c-- (ck-wrapper -))
(define-syntax c-/ (ck-wrapper /))

(define-syntax c-remainder (ck-wrapper remainder))
(define-syntax c-floor (ck-wrapper floor))
(define-syntax c-round (ck-wrapper round))

(define-syntax c-exact->inexact (ck-wrapper exact->inexact))
(define-syntax c-inexact->exact (ck-wrapper inexact->exact))

(define-syntax c-number->string (ck-wrapper number->string))
(define-syntax c-string->number (ck-wrapper string->number))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PAIRS AND LISTS

(define-syntax c-length (ck-wrapper length))
(define-syntax c-list-ref (ck-wrapper list-ref))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SYMBOLS

(define-syntax c-symbol? (ck-wrapper symbol?))
(define-syntax c-symbol->string (ck-wrapper symbol->string))
(define-syntax c-string->symbol (ck-wrapper string->symbol))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; CHARS AND STRINGS

(define-syntax c-char? (ck-wrapper char?))
(define-syntax c-char=? (ck-wrapper char=?))

(define-syntax c-string? (ck-wrapper string?))
(define-syntax c-string (ck-wrapper string))
(define-syntax c-string-length (ck-wrapper string-length))
(define-syntax c-string-ref (ck-wrapper string-ref))

(define-syntax c-string=? (ck-wrapper string=?))
(define-syntax c-string-ci=? (ck-wrapper string-ci=?))

(define-syntax c-substring (ck-wrapper substring))
(define-syntax c-string-append (ck-wrapper string-append))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; VECTORS

(define-syntax c-vector-length (ck-wrapper vector-length))
(define-syntax c-vector-ref (ck-wrapper vector-ref))
